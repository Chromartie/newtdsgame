// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "NewTDSGameMode.generated.h"

UCLASS(minimalapi)
class ANewTDSGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ANewTDSGameMode();
};



