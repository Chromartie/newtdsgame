// Copyright Epic Games, Inc. All Rights Reserved.

#include "NewTDSGameMode.h"
#include "NewTDSPlayerController.h"
#include "NewTDS/Char/NewTDSCharacter.h"
#include "UObject/ConstructorHelpers.h"

ANewTDSGameMode::ANewTDSGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ANewTDSPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}