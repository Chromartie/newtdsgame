// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Types.generated.h"


UENUM(BlueprintType)
enum class EMovementState : uint8 {
	EAimState UMETA(DisplayName = "Aim State"),
	EWalkState UMETA(DisplayName = "Walk State"),
	ERunState UMETA(DisplayName = "Run State"),
	ESprintState UMETA(DisplayName = "Sprint State")
};

USTRUCT(BlueprintType)
struct FCharacterSpeed {
	GENERATED_BODY()
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float WalkSpeed = 200.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float AimSpeed = 100.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float RunSpeed = 400.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float SprintSpeed = 750.0f;
};

UCLASS()
class NEWTDS_API UTypes : public UBlueprintFunctionLibrary {
	GENERATED_BODY()
/*public:
	Types();
	~Types();*/
};
