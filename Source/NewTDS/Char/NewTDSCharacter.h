// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "NewTDS/FunctionLibrary/Types.h"
#include "NewTDSCharacter.generated.h"

UCLASS(Blueprintable)
class ANewTDSCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ANewTDSCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* NewInputComponent) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
	FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** A decal that projects to the cursor location. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UDecalComponent* CursorToWorld;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	EMovementState MovementState = EMovementState::ERunState;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	FCharacterSpeed MovementInfo;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	bool bIsRunnig = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	bool bIsWalking = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	bool bIsAiming = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CameraSlide")
	float HeightChange = 50.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CameraSlide")
	float HeightMin = 400.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CameraSlide")
	float HeightMax = 1200.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CameraSlide")
	float SmoothStep = 0.001f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CameraSlide")
	float Step = 1.0f;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "CameraSlide")
	bool bUpSlide = true;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "CameraSlide")
	bool bSlide = true;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "CameraSlide")
	float CurrentSlideDist = 0.0f;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "CameraSlide")
	FTimerHandle SlideTimerHandle;

	UFUNCTION()
	void InputAxisX(float Value);
	
	UFUNCTION()
	void InputAxisY(float Value);

	float AxisX=0.0f;
	float AxisY=0.0f;

	UFUNCTION()
	void MovementTick(float DeltaTime);

	UFUNCTION(BlueprintCallable)
	void CharacterUpdate();

	UFUNCTION(BlueprintCallable)
	void ChangeMovementState();

	UFUNCTION(BlueprintCallable)
	void SlideSmooth(float Value);
	UFUNCTION(BlueprintCallable)
	void CameraSlideTimer();
};

