// Copyright Epic Games, Inc. All Rights Reserved.

#include "NewTDS.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, NewTDS, "NewTDS" );

DEFINE_LOG_CATEGORY(LogNewTDS)
 